all: test

test: test.o
	gcc -o test test.o -lm -lpthread

test.o: test.c
	gcc -c test.c 

clean:
	rm test *.o
	rm out.txt
