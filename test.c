#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <semaphore.h>

//threads can access global variables
int h1[4096] = {0};
int h2[4096] = {0};

int arraySize;			//total number of integers
int numOfRounds;		//number of rounds
int pick;				//if 1, write to  h1;if 2, write to h2
unsigned threadCount;
unsigned barrierCount;
int count;
int end;
int i;
int j;
int k;

pthread_mutex_t m;
//pthread_mutex_t m2;
pthread_cond_t cv;
//pthread_cond_t cv2;
sem_t s;

void *compare(void* index);	//to compare two integers and write back to array
void barrier();				//to wait until all threads are executed

int main(int argc, char *argv[]){
	if (argc != 2){
		fprintf(stderr, "need command line argument\n");
		return -1;
	}

	FILE* fp1;	//open input file
	FILE* fp2;	//open output file

	//conatenate the command line argument with ".txt"
	//this is for the output file
	char output[1024];
	strcpy(output, argv[1]);
	strcat(output, ".txt");

	fp1 = fopen("test.txt", "r");	//open input file to read
	fp2 = fopen(output, "w+");		//open output file to write

	char str[1024];		//string value read from fp1
	arraySize = 0;		
	numOfRounds = 0;

	//convert string to integers read from fp1 and store it to arrary
	while(fgets(str, 1024, fp1) != NULL){
		h1[arraySize] = atoi(str);
		arraySize++;
	}//end of while

	//log base 2 of arraySize = num of rounds
	numOfRounds = log((double)arraySize)/log(2);
	
	//init pthread
	pthread_t tid[2048];
	pthread_mutex_init(&m, NULL);
	//pthread_mutex_init(&m2, NULL);
	pthread_cond_init(&cv, NULL);
	//pthread_cond_init(&cv2, NULL);
	sem_init(&s, 0, 0);
	
	/* //check if array is correct
	int k;
	for(k = 0; k< 8; k++)
		printf("%d\n", h1[k]);
	*/

	if(arraySize == 0){
		printf("There is no integer in the file ");
	}
	else if(arraySize == 1){
		printf("%s %d\n", "The highest integer is ",h1[0] );
		fprintf(fp2, "%s %d\n", "The highest integer is ",h1[0]);
	}
	else if(arraySize > 1){
		pick =1;
		while(1){
			/*
			 1.create multiple threads and wait until each thread is executed
    		 2.wait until all threads are executed
    		 3.unlock the wait to do the next round
			*/
			threadCount = arraySize / 2;    //number of threads to create is half of the array size
            count = threadCount;            //count used in barrier for count--
            barrierCount = threadCount;
			printf("%s %d\n", "threadcount ", threadCount); 
			if(arraySize == 1){//no more rounds
				end = 1;
				break;
			}
			else if(threadCount > 1){//get out if arraySize becomes 1 after division (no more round needed)
				for(i = 0; i < threadCount; i++){//create multiple threads needed
					j = i * 2;
					//printf("%s %d\n", "j ", j);
					//create threadCount threads 
					//pthread_create(&tid, NULL, compare, (void*) index);
					pthread_create(&tid[i], NULL, compare, &j);
				}//end of for
				sem_wait(&s);	//block all worker threads
				arraySize = threadCount; //arraySize will be 1 when done
				printf("%s %d\n", "array size ", arraySize);
			}//end of else if
		}//end of while
	}//end of else if(arraySize > 1)
	if(pick == 1 && end){
		printf("%s %d\n", "The highest integer is h1",h1[0] );
        fprintf(fp2, "%s %d\n", "The highest integer is ",h1[0]);
	}//end of if
	else if(pick == 2 && end){
		printf("%s %d\n", "The highest integer is h2",h2[0]);
        fprintf(fp2, "%s %d\n", "The highest integer is ",h2[0]);
	}//end of else if
	return 0;
}//end of main

void *compare(void* index){
	int i = *((int *)index); //convert void pointer to int
	
	/*
	1. all of the integers are stored in h1
	2. after compare need to write to h2
	3. if read from h1, write to h2
	4. if read from h2, write to h1
	5. compare thread needs to call function barrier
	6. if the count inside barrier is not 0, wait
	7. if the count inside barrier is 0, broadcast to unlock all of the threads
	8. if 
	*/
	pthread_mutex_lock(&m);
	int q = i+1;
	//printf("%s %d\n", "index of array ", i);
	//printf("%s %d\n", "pick ", pick);
	if(pick == 2){//write to array h2
		if(h1[i] >= h1[q])
			h2[i/2] = h1[i];
		else
			h2[i/2] = h1[q];
		printf("%s %d\n", "h2 ", i);		
	}
	else if (pick == 1){//write to array h1
		if(h2[i] >= h2[q])
			h1[i/2] = h2[i];
		else
			h1[i/2] = h2[q];
		printf("%s %d\n","h1 ", i);	
	}		
	count--;
	printf("%d\n", count);
	barrier();	//to execute threads one by one
	pthread_mutex_unlock(&m);	
	sem_post(&s);	//to do the next round
}//end of compare

void barrier(){
	if(count !=0){
		printf("waiting\n");
		pthread_cond_wait(&cv, &m);	//wakes one thread among all of sleeping threads
	}
	else if (count == 0){
		printf("broadcasting\n");
		pthread_cond_broadcast(&cv);	//wakes all of the threads after all of the threads are done
		
		//round is over. need to switch array
		if (pick == 1)
			pick == 2;
		else if (pick == 2)
			pick == 1;
			//printf("%s %d\n", "pick isssss", pick);
	}
}//end of barrier



































































